import axios from 'axios'
import { MessageBox, Message } from 'element-ui'
import store from '@/store'
import { getToken } from '@/utils/auth'

// create an axios instance 创建axios实例
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url url=基本url+请求url
  // withCredentials: true 证书：真的, // send cookies when cross-domain requests  withCredentials:跨域请求时发送cookies
  timeout: 5000 // request timeout 请求超时
})

// request interceptor 请求拦截器
service.interceptors.request.use(
  config => {
    // do something before request is sent 在发送请求之前做些什么

    if (store.getters.token) {
      // let each request carry token 让每个请求都携带令牌
      // ['X-Token'] is a custom headers key 让每个请求携带令牌['X-token']是一个自定义密钥
      // please modify it according to the actual situation 请根据实际情况修改
      config.headers['X-Token'] = getToken()
    }
    return config
  },
  error => {
    // do something with request error 请根据实际情况进行修改，处理请求错误
    console.log(error) // for debug 用于调试
    return Promise.reject(error)
  }
)

// response interceptor  响应拦截器
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  response => response
  */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  response => {
    const res = response.data

    // if the custom code is not 20000, it is judged as an error. 如果自定义代码不是20000，则判断为错误。
    if (res.code !== 20000) {
      Message({
        message: res.message || 'Error',
        type: 'error',
        duration: 5 * 1000
      })

      // 50008: Illegal token; 50012: Other clients logged in; 50014: Token expired;
      if (res.code === 50008 || res.code === 50012 || res.code === 50014) {
        // to re-login
        MessageBox.confirm('您已注销，您可以取消以停留在此页，或重新登陆', '确认注销', {
          confirmButtonText: '重新登录',
          cancelButtonText: '取消',
          type: 'warning'
        }).then(() => {
          store.dispatch('user/resetToken').then(() => {
            location.reload()
          })
        })
      }
      return Promise.reject(new Error(res.message || 'Error'))
    } else {
      return res
    }
  },
  error => {
    console.log('err' + error) // for debug
    // Message({
    //   message: error.message,
    //   type: 'error',
    //   duration: 5 * 1000
    // })
    if (error.message.includes('timeout')) { // 判断请求异常信息中是否含有超时timeout字符串
      Message({
        message: '请求超时',
        type: 'error',
        duration: 5 * 1000
      })
      return Promise.reject(error)
    } else if (error.message.includes('Network')) {
      Message({
        message: '网络错误',
        type: 'error',
        duration: 5 * 1000
      })
      return Promise.reject(error)
    } else if (error.message.includes('404')) {
      Message({
        message: `请求失败，状态代码为404`,
        type: 'error',
        duration: 5 * 1000
      })
      return Promise.reject(error)
    } else if (error.message.includes('400')) {
      Message({
        message: `请求失败，状态代码为400`,
        type: 'error',
        duration: 5 * 1000
      })
      return Promise.reject(error)
    } else if (error.message.includes('500')) {
      Message({
        message: `请求失败，状态代码为500`,
        type: 'error',
        duration: 5 * 1000
      })
      return Promise.reject(error)
    } else {
      Message({
        message: error.message,
        type: 'error',
        duration: 5 * 1000
      })
    }
    return Promise.reject(error)
  }
)

export default service
