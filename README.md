
## Build Setup

```bash
# clone the project
git clone git@192.168.1.21:web/adminconsolefrontend.git

# enter the project directory
cd adminconsolefrontend
# install dependency  
npm install

# develop
npm run dev
```

This will automatically open http://localhost:9528

## Build

```bash
# build for test environment
npm run build:stage

# build for production environment
npm run build:prod
```

## Advanced

```bash
# preview the release environment effect
npm run preview

# preview the release environment effect + static resource analysis
npm run preview -- --report

# code format check
npm run lint

# code format check and auto fix
npm run lint -- --fix
```
